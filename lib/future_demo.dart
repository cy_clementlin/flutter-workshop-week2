
import 'dart:math';

import 'package:flutter/material.dart';

class FutureDemoPage extends StatefulWidget {
  @override
  _FutureDemoPageState createState() => _FutureDemoPageState();
}

class _FutureDemoPageState extends State<FutureDemoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            OutlineButton(
              child: Text('Future.forEach'),
              onPressed: () => _forEach(),
            ),
            OutlineButton(
              child: Text('Future.delayed'),
              onPressed: () => _doDelay(),
            ),
            OutlineButton(
              child: Text('Future.wait'),
              onPressed: () => _doWait(),
            ),
            OutlineButton(
              child: Text('Future.doWhile'),
              onPressed: () => _doWhile(),
            ),
          ],
        ),
      ),
    );
  }

  _forEach() async {
    Future.forEach([1, 2, 3, 4], (i) {
      return Future.delayed(Duration(seconds: i), () => debugPrint('output: $i'));
    });
  }

  _doDelay() async {
    var output = await Future.delayed(Duration(seconds: 2), () => 'delayed');
    debugPrint('output: $output');
  }

  _doWait() async {
    var future1 = new Future.delayed(new Duration(seconds: 1), () => 1);
    var future2 =
    new Future.delayed(new Duration(seconds: 2), () => 2);
    var future3 = new Future.delayed(new Duration(seconds: 3), () => 3);
    Future.wait({future1,future2,future3}).then(print).catchError(print);
  }

  _doWhile() async {

    Future.doWhile(() {
      /**
       * !!!動手試試看!!!
       * 隨機生成秒數(使用_getRandomDelay), 並等待(使用_delayBySec)
       * 直到 總等待秒數超過十秒 結束doWhile
       * return true 繼續, return false 結束
       */

      return false;
    })
        .then(print)
        .catchError(print);
  }

  int _getRandomDelay() {
    var random = Random();
    return random.nextInt(5);
  }

  Future<bool> _delayBySec(int sec) {
    return Future.delayed(Duration(seconds: sec), () {
      debugPrint('delayed : $sec sec.');
      return true;
    });
  }
}
