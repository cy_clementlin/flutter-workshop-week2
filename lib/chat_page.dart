
import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';

class Msg {
  bool isLeft;
  String msg = '';

  Msg({this.isLeft = false, @required this.msg});
}

class ChatPage extends StatefulWidget {
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  //color
  final TextStyle _titleTextStyle = const TextStyle(color: Colors.white, fontSize: 18.0);
  final Color blackColor = Color(0xff19191b);
  final Color greyColor = Color(0xff8f8f8f);
  final Color separatorColor = Color(0xff272c35);
  static final Color gradientColorStart = Color(0xff00b6f3);
  static final Color gradientColorEnd = Color(0xff0184dc);
  final Gradient fabGradient = LinearGradient(
      colors: [gradientColorStart, gradientColorEnd],
      begin: Alignment.topLeft,
      end: Alignment.bottomRight);

  //component
  TextEditingController textFieldController = TextEditingController();
  ScrollController _scrollController = new ScrollController();

  //msg
  List<Msg> _msgList = List();

  /// * 需求
  /// 1，將聊天室改為異步更新版本
  /// 2，封裝訊息處理功能(msg_bloc)

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: blackColor,
      appBar: _buildAppbar(context),
      body: Column(
        children: [
          Flexible(child: _buildMessageList(context)),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Row(
              children: [
                SizedBox(width: 5,),
                Expanded(
                  child: TextField(
                    controller: textFieldController,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    onChanged: (val) {},
                    decoration: InputDecoration(
                      hintText: "Type a message",
                      hintStyle: TextStyle(
                        color: greyColor,
                      ),
                      border: OutlineInputBorder(
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(50.0),
                          ),
                          borderSide: BorderSide.none),
                      contentPadding:
                      EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                      filled: true,
                      fillColor: separatorColor,
                      suffixIcon: GestureDetector(
                        onTap: () {},
                        child: Icon(Icons.face),
                      ),
                    ),
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(left: 10),
                    decoration: BoxDecoration(
                        gradient: fabGradient,
                        shape: BoxShape.circle),
                    child: IconButton(
                      icon: Icon(
                        Icons.send,
                        size: 20,
                      ),
                      onPressed: () {
                        if(textFieldController.text.isEmpty) return;

                        _updateList(Msg(isLeft: false, msg: textFieldController.text));
                      },
                    ))
              ],
            ),
          )
        ],
      ),
    );
  }

  _updateList(Msg msg) {
    setState(() {
      _msgList.add(msg);
      _moveToBottom();
    });
  }

  _buildAppbar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.transparent,
      leading: Icon(Icons.arrow_back, color: Colors.white,),
      centerTitle: true,
      title: Text('Chat', style: _titleTextStyle,),
    );
  }

  //移至底部
  void _moveToBottom() {
    _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
  }

  //========================================
  //=== 建立聊天室
  //========================================
  _buildMessageList(BuildContext context) {
    return ListView.builder(
      controller: _scrollController,
      itemCount: _msgList.length,
      itemBuilder: (context, index) {
          return _buildMessageItem(_msgList[index]);
      }
    );
  }

  _buildMessageItem(Msg msg) {
    return Column(
      children: [
        Bubble(
          margin: BubbleEdges.only(top: 10),
          nip: msg.isLeft ? BubbleNip.leftTop : BubbleNip.rightTop,
          alignment: Alignment.topRight,
          color: Color.fromRGBO(225, 255, 199, 1.0),
          child: Text(msg.msg, textAlign: TextAlign.right),
        ),
        SizedBox(height: 10,)
      ],
    );
  }
}
