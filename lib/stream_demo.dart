import 'dart:async';

import 'package:flutter/material.dart';

class StreamDemoPage extends StatefulWidget {
  @override
  _StreamDemoPageState createState() => _StreamDemoPageState();
}

class _StreamDemoPageState extends State<StreamDemoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            OutlineButton(
              child: Text('Stream.take'),
              onPressed: () => _take(),
            ),
            OutlineButton(
              child: Text('Stream.takeWhile'),
              onPressed: () => _takeWhile(),
            ),
            OutlineButton(
              child: Text('Stream.map'),
              onPressed: () => _map(),
            ),
            OutlineButton(
              child: Text('StreamController'),
              onPressed: () => _streamController(),
            ),
          ],
        ),
      ),
    );
  }

  _take() async {
    Duration interval = Duration(seconds: 1);

    //周期性事件
    Stream<int> stream = Stream.periodic(interval, (data) => data);

    //只取10個摙件
    stream = stream.take(10);

    await for (int i in stream) {
      print(i);
    }
  }

  _takeWhile() async {
    Duration interval = Duration(seconds: 1);

    Stream<int> stream = Stream.periodic(interval, (data) => data);

    stream = stream.takeWhile((data) {
      return data < 10;
    });

    await for (int i in stream) {
      print(i);
    }
  }

  _map() async {
    Duration interval = Duration(seconds: 1);

    Stream<int> stream = Stream<int>.periodic(interval, (data) => data);

    stream = stream.map((data) => data + 1);

    await for (int i in stream) {
      print(i);
    }
  }

  _streamController() {
    StreamController<String> streamController = StreamController();

    streamController.stream.listen(
            (data) => print(data),
            onError: (e) => print('onError'),
            onDone:  () => print('onDone')
    );

    streamController.sink.add("aaa");
    streamController.add("bbb");
    streamController.add("ccc");
    streamController.addError(Exception('a fail'));
    streamController.close();
  }
}
